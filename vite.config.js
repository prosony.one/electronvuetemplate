import { fileURLToPath, URL } from 'url'
import vue from '@vitejs/plugin-vue'

module.exports = {
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src/frontend', import.meta.url))
    }
  },
  root: './src/frontend',
  base: '',
  build: {
    // inputs: './src/frontend/',
    outDir: '../../dist',
    output: {
      assetFileNames: (assetInfo) => {
        // let extType = assetInfo.name.split('.')[1]; // let extType = assetInfo.name.split('.').at(1);
        
        // if (/png|jpe?g|svg|gif|tiff|bmp|ico/i.test(extType)) {
        //   extType = 'img';
        // }
        // return `[name][extname]`;
        // return `src/frontend/assets/[name][extname]`;
        // return `src/frontend/assets/${extType}/[name][extname]`;

        return `[name][extname]`;
      },
    }
  }
}